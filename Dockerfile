FROM debian
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN apt-get install -y python-pip
RUN pip install allure-pytest

RUN apt-get -y install default-jre

ADD ./allure-2.10.0 .

RUN export PATH=allure-2.10.0/bin:$PATH

EXPOSE 5050
CMD tail -f /dev/null